============
Money parser
============

Parse exports of bank accounts and save them in an unified format to pandas, csv, sqlite, ...

Quickstart
==========

* python3.10 -m venv env
* . env/bin/activate
* pip install -r requirements.txt
* python -m money_parser.main run --help


Supported import formats
========================

* easybank csv export
* SGKB csv export


Supported export formats
========================

* json
* csv
* homebank csv
* SQL (sqlite, postgres, ...) [TBD]


Constraints
===========

Only clean UTF-8 (without useless BOM) files are supported, convert input files before importing.

Example iconv settings:

easybank: *iconv infile.csv -f ISO-8859-1 -o outfile.csv*

SGKB: *sed -i 's/\xef\xbb\xbf//' infile.csv*
