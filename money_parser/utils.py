from pathlib import Path
from typing import Optional, Type

from .exporter import BaseExporter, CsvExporter, HomebankExporter, JsonExporter, SqlExporter
from .importer import CsvImporter, EasybankImporter, RevolutImporter, SgkbImporter

IMPORTER_CLASSES: dict[str, Type] = {
    'easybank': EasybankImporter,
    'sgkb': SgkbImporter,
    'revolut': RevolutImporter
}

EXPORTER_CLASSES: dict[str, Type] = {
    'json': JsonExporter,
    'sql': SqlExporter,
    'csv': CsvExporter,
    'homebank': HomebankExporter
}


def get_importer(importer_name: str) -> CsvImporter:
    Importer: Optional[Type[CsvImporter]] = IMPORTER_CLASSES.get(
        importer_name)
    assert Importer is not None
    return Importer()


def get_exporter(exporter_name: str, path: str | Path) -> BaseExporter:
    Exporter: Optional[Type[BaseExporter]] = EXPORTER_CLASSES.get(
        exporter_name)
    assert Exporter is not None
    return Exporter(path)
