from enum import Enum
from pathlib import Path
from typing import Optional

import typer

from .models import Account
from .utils import get_exporter, get_importer

app = typer.Typer()


class Importer(str, Enum):
    easybank = 'easybank'
    sgkb = 'sgkb'
    revolut = 'revolut'


class Exporter(str, Enum):
    json = 'json'
    sql = 'sql'
    csv = 'csv'
    homebank = 'homebank'


@app.command()
def import_csv(
  source: Path, source_format: Importer, target: Path, target_format: Exporter,
  account: Optional[str] = None, currency: Optional[str] = None,
  name: Optional[str] = None
):
    importer = get_importer(source_format)
    exporter = get_exporter(target_format, target)

    assert source.exists()

    with source.open() as f:
        entries, extra = importer.load_file(f)

    if account is None:
        account = extra.get('account_number', 'UNKNOWN')

    if currency is None:
        currency = extra.get('currency', 'EUR')

    account_obj = Account(
        id=account,
        name=name,
        currency=currency,
        entries=entries
    )
    typer.echo('Creating Account')
    exporter.create_or_upgrade_account(account_obj)

    label = f'Inserting {len(account_obj.entries)} entries'
    with typer.progressbar(account_obj.entries, label=label) as progress:
        try:
            progress.short_limit = 0  # type: ignore
        except AttributeError:
            pass

        exporter.insert_entries(progress)

    typer.echo('Sorting')
    exporter.sort()

    with typer.progressbar(account_obj.entries, label='Saving') as progress:
        try:
            progress.short_limit = 0  # type: ignore
        except AttributeError:
            pass
        exporter.commit(progress.update)


if __name__ == '__main__':
    app()
