import datetime
from decimal import Decimal
from typing import Optional

from pydantic import BaseModel


class AccountEntry(BaseModel):
    id: str
    date: datetime.date
    valuta: Optional[datetime.date]
    text: str
    amount: Decimal
    balance: Optional[Decimal]


class Account(BaseModel):
    id: str
    name: Optional[str]
    entries: list[AccountEntry]
    currency: str
