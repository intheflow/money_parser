from typing import Callable, Iterable, Optional

from ..models import Account, AccountEntry
from . import BaseExporter


class SqlExporter(BaseExporter):

    def __init__(self, path: str):
        ...

    def insert_entry(self, entry: AccountEntry) -> bool:
        ...

    def insert_entries(self, entries: Iterable[AccountEntry]) -> list[bool]:
        ...

    def create_or_upgrade_account(self, account: Account) -> Optional[bool]:
        ...

    def sort(self, field: str = 'date', reverse: bool = True):
        ...

    def commit(self, progress_func: Optional[Callable] = None):
        ...
