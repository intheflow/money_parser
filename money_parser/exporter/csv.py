from csv import DictReader, DictWriter
from pathlib import Path
from typing import Callable, Optional, TextIO, Type

from ..importer import CsvImporter
from ..models import AccountEntry
from .base import BaseExporter


class CsvExporter(BaseExporter):
    FIELDNAMES: list[str] = [
        'id', 'date', 'valuta', 'text', 'amount', 'balance'
    ]
    MAPPINGS: dict[str, str] = {}
    FALLBACK: dict = {'valuta': 'date'}
    DIALECT = 'excel'
    DELIMITER = ';'
    SKIP_FIRST_LINE = False
    WRITE_HEADER = False
    IMPORTER_CLASS: Type[CsvImporter] = CsvImporter

    def __init__(self, path: str | Path):
        super().__init__(path)
        self.importer = self.IMPORTER_CLASS()

        self.REVERSE_MAPPINGS = {
            value: key for key, value in self.MAPPINGS.items()
        }

        if self.path.exists() and self.path.stat().st_size:
            with self.path.open() as f:
                csv_data = self.load_csv(f)
                for entry_dict in csv_data:
                    entry = self.load_entry(entry_dict)
                    self.entries.append(AccountEntry(**entry))

    def load_csv(self, data: TextIO) -> DictReader:
        if self.SKIP_FIRST_LINE:
            data.readline()
        return DictReader(
            data, fieldnames=self.FIELDNAMES, dialect=self.DIALECT,
            delimiter=self.DELIMITER
        )

    def load_entry(self, entry_dict: dict[str, str]):
        return self.importer.load_entry(entry_dict)

    def dump_entry(self, entry_dict: dict[str, str]) -> dict:
        """ Convert AccountEntry to target format
        """
        result_dict: dict[str, str] = {}
        for fieldname in self.FIELDNAMES:
            mapped_fieldname = self.REVERSE_MAPPINGS.get(fieldname, fieldname)
            field = entry_dict.get(mapped_fieldname)
            if field is None and (fallback := self.FALLBACK.get(fieldname)):
                field = entry_dict.get(fallback)
            if field is not None:
                result_dict[fieldname] = field

        return result_dict

    def insert_entry(self, entry: dict | AccountEntry) -> bool:
        entry_dict: dict
        if isinstance(entry, AccountEntry):
            entry_dict = entry.dict()
        else:
            entry_dict = entry

        cleaned_entry = self.load_entry(self.dump_entry(entry_dict))
        if cleaned_entry not in self.entries:
            self.entries.append(AccountEntry(**cleaned_entry))
            return True
        return False

    def commit(self, progress_func: Optional[Callable] = None):
        with self.path.open('w') as f:
            writer = DictWriter(
                f, self.FIELDNAMES, dialect=self.DIALECT,
                delimiter=self.DELIMITER
            )
            if self.WRITE_HEADER:
                writer.writeheader()
            for entry in self.entries:
                writer.writerow(self.dump_entry(entry.dict()))
                if progress_func is not None:
                    progress_func(1)
