from ..importer import HomebankImporter
from .csv import CsvExporter


class HomebankExporter(CsvExporter):
    FIELDNAMES: list[str] = [
        'date', 'payment mode', 'info', 'payee',
        'memo', 'amount', 'category', 'tags'
    ]
    MAPPINGS = {
        'text': 'info'
    }
    IMPORTER_CLASS = HomebankImporter
