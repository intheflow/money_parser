from abc import ABC
from operator import attrgetter
from pathlib import Path
from typing import Callable, Iterable, Optional

from ..models import Account, AccountEntry


class BaseExporter(ABC):

    def __init__(self, path: str | Path):
        self.path = Path(path)
        self.entries: list[AccountEntry] = []
        self.account: Optional[Account] = None

    def insert_entry(self, entry: AccountEntry) -> bool:
        raise NotImplementedError

    def insert_entries(self, entries: Iterable[AccountEntry]) -> list[bool]:
        inserted: list[bool] = []
        for entry in entries:
            inserted.append(self.insert_entry(entry))
        return inserted

    def create_or_upgrade_account(self, account: Account) -> Optional[bool]:
        self.account = account
        return True

    def sort(self, field: str = 'date', reverse: bool = True):
        self.entries.sort(key=attrgetter(field), reverse=reverse)

    def commit(self, progress_func: Optional[Callable] = None):
        raise NotImplementedError
