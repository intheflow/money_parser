import json
from datetime import date
from pathlib import Path
from typing import Callable, Optional

from ..models import Account, AccountEntry
from . import BaseExporter


def json_serial(obj):
    if isinstance(obj, date):
        return obj.isoformat()
    return str(obj)


class JsonExporter(BaseExporter):

    def __init__(self, path: str | Path):
        super().__init__(path)
        self.path = Path(path)
        if self.path.exists() and self.path.stat().st_size:
            with self.path.open() as f:
                self.data = Account(**json.load(f)).dict()
        else:
            self.data = {}

    def insert_entry(self, entry: AccountEntry) -> bool:
        if entry not in self.entries:
            self.entries.append(entry)
            return True
        return False

    def create_or_upgrade_account(self, account: Account) -> Optional[bool]:
        account_data = account.dict()
        if 'entries' in account_data:
            account_data.pop('entries')
        self.data.update(account_data)

        if 'entries' not in self.data:
            self.data['entries'] = self.entries
            return True
        return None

    def commit(self, progress_func: Optional[Callable] = None):
        assert Account.validate(self.data)
        with self.path.open('w') as f:
            json.dump(self.data, f, default=json_serial)
            if progress_func is not None:
                progress_func(len(self.entries))
