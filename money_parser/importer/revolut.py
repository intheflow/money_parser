import logging
from typing import Optional, TextIO

from ..models import AccountEntry
from . import CsvImporter
from .types import FIELD_VALUE

logger = logging.getLogger()


class RevolutImporter(CsvImporter):
    FIELDNAMES = [
        'Type', 'Product', 'Started Date', 'Completed Date', 'Description',
        'Amount', 'Fee', 'Currency', 'State', 'Balance'
    ]
    DELIMITER = ','
    SKIP_FIRST_LINE = True
    DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

    def load_file(self, data: TextIO) -> tuple[list[AccountEntry], dict[str, str]]:
        content = self.load_csv(data)
        entries: list[AccountEntry] = []

        for line in content:
            amount = self.clean_decimal(line['Amount'])
            if amount is None:
                logger.warning(f'Invalid amount in entry: {line}')
                continue
            if fee := self.clean_decimal(line['Fee']):
                amount -= abs(fee)
            else:
                logger.warning(f'Invalid fee in entry: {line}')

            date = self.convert_date(line['Completed Date'])
            valuta = self.convert_date(line['Started Date'])

            entry: dict[str, Optional[FIELD_VALUE]] = {
                'date': date or valuta,
                'valuta': valuta,
                'text': line['Description'].strip(),
                'amount': amount,
                'balance': self.clean_decimal(line['Balance'])
            }
            entries.append(AccountEntry(id=self.generate_id(entry), **entry))

        return entries, {}
