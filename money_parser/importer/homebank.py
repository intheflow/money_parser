from typing import Optional, TextIO

from ..models import AccountEntry
from . import CsvImporter
from .types import FIELD_VALUE


class HomebankImporter(CsvImporter):
    FIELDNAMES: list[str] = [
        'date', 'payment mode', 'info', 'payee',
        'memo', 'amount', 'category', 'tags'
    ]
    DECIMAL_CHAR = ','
    THOUSAND_SEPARATOR = '.'

    def load_entry(self, entry_dict: dict[str, str]):
        entry: dict[str, Optional[FIELD_VALUE]] = {
            'date': entry_dict['date'],
            'text': entry_dict['info'].strip(),
            'amount': entry_dict['amount']
        }
        entry['id'] = self.generate_id(entry)

        return entry

    def load_file(self, data: TextIO):
        content = self.load_csv(data)
        entries: list[AccountEntry] = []
        extra: dict[str, str] = {}

        for line in content:
            entries.append(AccountEntry(**self.load_entry(line)))

        return entries, extra
