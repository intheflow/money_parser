import re
from typing import Optional, TextIO

from ..models import AccountEntry
from . import CsvImporter
from .types import FIELD_VALUE

RE_TRANSACTION_ID = re.compile(r'[A-Z]{2}/\d{9}')


class EasybankImporter(CsvImporter):
    FIELDNAMES = [
        'account_number', 'text', 'date', 'valuta', 'amount', 'currency'
    ]
    DECIMAL_CHAR = ','
    THOUSAND_SEPARATOR = '.'

    def load_file(self, data: TextIO) -> tuple[list[AccountEntry], dict[str, str]]:  # NOQA: E501
        content = self.load_csv(data)
        entries: list[AccountEntry] = []
        extra: dict[str, str] = {}

        entry: dict[str, Optional[FIELD_VALUE]]
        for i, line in enumerate(content):
            if i == 0:
                extra['account_number'] = line['account_number']
                extra['currency'] = line['currency']
            entry = {
                'date': self.convert_date(line['date']),
                'valuta': self.convert_date(line['valuta']),
                'text': line['text'].strip(),
                'amount': self.clean_decimal(line['amount'])
            }
            tid_match = RE_TRANSACTION_ID.search(line['text'])
            if tid_match:
                entry['id'] = tid_match.group()
            else:
                entry['id'] = self.generate_id(entry)
            entries.append(AccountEntry(**entry))

        return entries, extra
