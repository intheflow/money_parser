from typing import Optional, TextIO

from ..models import AccountEntry
from . import CsvImporter
from .types import FIELD_VALUE


class SgkbImporter(CsvImporter):
    FIELDNAMES = [
        'date', 'valuta', 'text', 'debit', 'credit', 'balance'
    ]
    DECIMAL_CHAR = '.'
    THOUSAND_SEPARATOR = "'"
    SKIP_FIRST_LINE = True

    def load_file(self, data: TextIO) -> tuple[list[AccountEntry], dict[str, str]]:  # NOQA: E501
        content = self.load_csv(data)
        entries: list[AccountEntry] = []

        for line in content:
            amount_str = line['credit'] or f'-{line["debit"]}'
            amount = self.clean_decimal(amount_str)
            entry: dict[str, Optional[FIELD_VALUE]] = {
                'date': self.convert_date(line['date']),
                'valuta': self.convert_date(line['valuta']),
                'text': line['text'].strip(),
                'amount': amount,
                'balance': self.clean_decimal(line['balance'])
            }
            entries.append(AccountEntry(id=self.generate_id(entry), **entry))

        return entries, {}
