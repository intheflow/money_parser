import logging
from abc import ABC
from csv import DictReader
from datetime import date, datetime
from decimal import Decimal, InvalidOperation
from hashlib import sha1 as hash_func
from typing import Optional, TextIO

from ..models import AccountEntry
from .types import FIELD_VALUE

logger = logging.getLogger()


class CsvImporter(ABC):
    FIELDNAMES: Optional[list[str]] = None
    DIALECT = 'excel'
    DELIMITER = ';'
    DATE_FORMAT = '%d.%m.%Y'
    DECIMAL_CHAR = '.'
    THOUSAND_SEPARATOR = ','
    SKIP_FIRST_LINE = False

    def load_csv(self, data: TextIO) -> DictReader:
        if self.SKIP_FIRST_LINE:
            data.readline()
        return DictReader(
            data, fieldnames=self.FIELDNAMES, dialect=self.DIALECT,
            delimiter=self.DELIMITER
        )

    def load_entry(self, entry_dict: dict[str, str]) -> dict[str, Optional[FIELD_VALUE]]:  # NOQA: E501
        """ Convert entry from source format to internal AccountEntry format
        """
        entry: dict[str, Optional[FIELD_VALUE]] = {
            'id': entry_dict['id'],
            'date': self.convert_date(entry_dict['date']),
            'text': entry_dict['text'].strip(),
            'amount': self.clean_decimal(entry_dict['amount'])
        }
        if 'valuta' in entry_dict:
            entry['valuta'] = self.convert_date(entry_dict['valuta'])
        if 'balance' in entry_dict:
            entry['balance'] = self.clean_decimal(entry_dict['balance'])

        return entry

    def load_file(self, data: TextIO) -> tuple[list[AccountEntry], dict[str, str]]:  # NOQA: E501
        content = self.load_csv(data)
        entries: list[AccountEntry] = []
        extra: dict[str, str] = {}

        for line in content:
            entries.append(AccountEntry(**self.load_entry(line)))

        return entries, extra

    def convert_date(self, date_str: str | date) -> Optional[date]:
        if isinstance(date_str, date):
            return date_str

        try:
            return datetime.strptime(date_str, self.DATE_FORMAT).date()
        except ValueError:
            logger.warning(f'Unable to convert {date_str} to date')
            return None

    def clean_decimal(self, decimal_str: str | Decimal) -> Optional[Decimal]:
        if isinstance(decimal_str, Decimal):
            return decimal_str
        decimal_str = decimal_str.replace('+', '')
        decimal_str = decimal_str.replace(self.THOUSAND_SEPARATOR, '')
        decimal_str = decimal_str.replace(self.DECIMAL_CHAR, '.')
        try:
            return Decimal(decimal_str)
        except InvalidOperation:
            logger.warning(f'Unable to convert {decimal_str} to decimal')
            return None

    def generate_id(self, data: dict[str, Optional[FIELD_VALUE]]) -> str:
        data_str = str(data)
        return hash_func(data_str.encode()).hexdigest()
